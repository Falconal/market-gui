import React, { Component } from 'react';

class ProductApp extends Component {

    constructor(){
        super();
        this.state = {
            name: '',
            quantity: 0,
            category: '',
            price: 0.00,
            products: [],
            _id: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.addProduct = this.addProduct.bind(this);
    }

    addProduct(e){
        if (this.state._id) {
            fetch(`/api/products/${this.state._id}`, {
                method: 'PUT',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    M.toast({html: 'Producto Actualizado'});
                    this.setState({ 
                        name: '',
                        quantity: 0,
                        category: '',
                        price: 0.00,
                        _id: ''
                    });
                })
                .catch(err => console.error(err));
                this.fetchProducts();
        } else {
            fetch('/api/products',{
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    M.toast({html: 'Producto Guardado'});
                    this.setState({ 
                        name: '',
                        quantity: 0,
                        category: '',
                        price: 0.00,
                        _id: ''
                    });
                })
                .catch(err => console.error(err));
                this.fetchProducts();
        }
        e.preventDefault();
    }

    componentDidMount() {
        this.fetchProducts();
    }

    fetchProducts() {
        fetch('/api/products')
            .then(res => res.json())
            .then(data => {
                this.setState({products: data});
                console.log(this.state.products);
            })
    }

    deleteProduct(id) {
        if (confirm('Seguro de querer eliminarlo?')) {   
            fetch(`api/products/${id}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    M.toast({html: 'Producto Eliminado'});
                    this.fetchProducts();
                })
        }
    }

    editProduct(id) {
           
        fetch(`api/products/${id}`)         
            .then(res => res.json())
            .then(data => {
                console.log(data)
                this.setState({ 
                    name: data.name,
                    quantity: data.quantity,
                    category: data.category,
                    price: data.price,
                    _id: data._id
                })
            });
    }
    
    handleChange(e){
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }

    render(){
        return (
            <div>
                { /* Navigation */}
                <nav className="light-blue darken 4">
                    <div className="container">
                        <a className="brand-logo" href="/">Volver al Inicio</a>
                    </div>
                </nav>

                <div className="container">
                    <div className="row">
                        <div className="col s4">
                            <div className="card">
                                <div className="card-content">
                                    <form onSubmit={this.addProduct}>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <input name="name" onChange={this.handleChange}
                                                type="text" value={this.state.name} 
                                                placeholder="Nombre del Producto"/>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="quantity" onChange={this.handleChange} 
                                                placeholder="Cantidad" 
                                                className="materialize-textarea" value={this.state.quantity}></textarea>
                                            </div>          
                                        </div>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="category" onChange={this.handleChange} 
                                                placeholder="Categoría" 
                                                className="materialize-textarea" value={this.state.category}></textarea>
                                            </div>          
                                        </div>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="price" onChange={this.handleChange} 
                                                placeholder="Precio del Producto" 
                                                className="materialize-textarea" value={this.state.price}></textarea>
                                            </div>          
                                        </div>

                                        <button type="submit" className="btn light-blue darken 4">
                                                Guardar
                                        </button>
                                    </form>
                                </div>
                            </div>
                        
                        </div>
                        <div className="col s14">
                        
                        <table>
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Cantidad</th>
                                    <th>Categoría</th>
                                    <th>Precio</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.products.map(product => {
                                        return (
                                            <tr key={product._id}>
                                                <td>{product.name}</td>
                                                <td>{product.quantity}</td>
                                                <td>{product.category}</td>
                                                <td>{product.price}</td>
                                                <td>
                                                    <button className="btn light-blue darken 4"
                                                    onClick={() => this.deleteProduct(product._id)}>
                                                        <i className="material-icons">delete</i>
                                                    </button>
                                                    <button onClick={() => this.editProduct(product._id)} 
                                                    className="btn light-blue darken 4"
                                                    style={{margin:'4px'}}>
                                                        <i className="material-icons">edit</i>
                                                    </button>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>

                        </table>

                        </div>

                    </div>
                </div>
            </div>
        )
    }

}

export default ProductApp;