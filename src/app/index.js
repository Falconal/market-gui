import React from 'react';
import { render } from 'react-dom';

import ClientApp from './ClientApp';
import ProductApp from './ProductApp';
import StaffApp from './StaffApp';

render(<ClientApp/>, document.getElementById('clientApp'));
render(<ProductApp/>, document.getElementById('productApp'));
render(<StaffApp/>, document.getElementById('staffApp'));