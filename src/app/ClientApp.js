import React, { Component } from 'react';

class ClientApp extends Component {

    constructor(){
        super();
        this.state = {
            name: '',
            id_n: '',
            age: 0,
            gender: '',
            birthdate: '',
            address: '',
            clients: [],
            _id: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.addClient = this.addClient.bind(this);
    }

    addClient(e){
        if (this.state._id) {
            fetch(`/api/clients/${this.state._id}`, {
                method: 'PUT',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    M.toast({html: 'Cliente Actualizado'});
                    this.setState({ 
                        name: '',
                        id_n: '',
                        age: 0,
                        gender: '',
                        birthdate: '',
                        address: '',
                        _id: ''
                    });
                })
                .catch(err => console.error(err));
                this.fetchClients();
        } else {
            fetch('/api/clients',{
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    M.toast({html: 'Cliente Guardado'});
                    this.setState({ 
                        name: '',
                        id_n: '',
                        age: 0,
                        gender: '',
                        birthdate: '',
                        address: ''
                    });
                })
                .catch(err => console.error(err));
                this.fetchClients();
        }
        e.preventDefault();
    }

    componentDidMount() {
        this.fetchClients();
    }

    fetchClients() {
        fetch('/api/clients')
            .then(res => res.json())
            .then(data => {
                this.setState({clients: data});
                console.log(this.state.clients);
            })
    }

    deleteClient(id) {
        if (confirm('Seguro de querer eliminarlo?')) {   
            fetch(`api/clients/${id}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    M.toast({html: 'Cliente Eliminado'});
                    this.fetchClients();
                })
        }
    }

    editClient(id) {
           
        fetch(`api/clients/${id}`)         
            .then(res => res.json())
            .then(data => {
                console.log(data)
                this.setState({
                    name: data.name,
                    id_n: data.id_n,
                    age: data.age,
                    gender: data.gender,
                    birthdate: data.birthdate,
                    address: data.address,
                    _id: data._id
                })
            });
    }
    
    handleChange(e){
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }

    render(){
        return (
            <div>
                { /* Navigation */}
                <nav className="light-blue darken 4">
                    <div className="container">
                        <a className="brand-logo" href="/">Volver al Inicio</a>
                    </div>
                </nav>

                <div className="container">
                    <div className="row">
                        <div className="col s4">
                            <div className="card">
                                <div className="card-content">
                                    <form onSubmit={this.addClient}>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <input name="name" onChange={this.handleChange}
                                                type="text" value={this.state.name} 
                                                placeholder="Nombre del Cliente"/>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="id_n" onChange={this.handleChange} 
                                                placeholder="Cedula del Cliente" 
                                                className="materialize-textarea" value={this.state.id_n}></textarea>
                                            </div>          
                                        </div>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="age" onChange={this.handleChange} 
                                                placeholder="Edad" 
                                                className="materialize-textarea" value={this.state.age}></textarea>
                                            </div>          
                                        </div>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="gender" onChange={this.handleChange} 
                                                placeholder="Genero" 
                                                className="materialize-textarea" value={this.state.gender}></textarea>
                                            </div>          
                                        </div>
                                        
                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="birthdate" onChange={this.handleChange} 
                                                placeholder="Fecha de Nacimiento del Cliente" 
                                                className="materialize-textarea" value={this.state.birthdate}></textarea>
                                            </div>          
                                        </div>
                                        
                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="address" onChange={this.handleChange} 
                                                placeholder="Direccion del Cliente" 
                                                className="materialize-textarea" value={this.state.address}></textarea>
                                            </div>          
                                        </div>

                                        <button type="submit" className="btn light-blue darken 4">
                                                Guardar
                                        </button>
                                    </form>
                                </div>
                            </div>
                        
                        </div>
                        <div className="col s14">
                        
                        <table>
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Cedula</th>
                                    <th>Edad</th>
                                    <th>Genero</th>
                                    <th>Fecha de Nacimiento</th>
                                    <th>Direccion</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.clients.map(client => {
                                        return (
                                            <tr key={client._id}>
                                                <td>{client.name}</td>
                                                <td>{client.id_n}</td>
                                                <td>{client.age}</td>
                                                <td>{client.gender}</td>
                                                <td>{client.birthdate}</td>
                                                <td>{client.address}</td>
                                                <td>
                                                    <button className="btn light-blue darken 4"
                                                    onClick={() => this.deleteClient(client._id)}>
                                                        <i className="material-icons">delete</i>
                                                    </button>
                                                    <button onClick={() => this.editClient(client._id)} 
                                                    className="btn light-blue darken 4"
                                                    style={{margin:'4px'}}>
                                                        <i className="material-icons">edit</i>
                                                    </button>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>

                        </table>

                        </div>

                    </div>
                </div>
            </div>
        )
    }

}

export default ClientApp;