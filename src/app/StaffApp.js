import React, { Component } from 'react';

class StaffApp extends Component {

    constructor(){
        super();
        this.state = {
            name: '',
            id_n: '',
            age: 0,
            gender: '',
            birthdate: '',
            address: '',
            user: '',
            password: '',
            email: '',
            staff: [],
            _id: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.addStaff = this.addStaff.bind(this);
    }

    addStaff(e){
        if (this.state._id) {
            fetch(`/api/staff/${this.state._id}`, {
                method: 'PUT',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    M.toast({html: 'Miembro del Personal Actualizado'});
                    this.setState({ 
                        name: '',
                        id_n: '',
                        age: 0,
                        gender: '',
                        birthdate: '',
                        address: '',
                        user: '',
                        password: '',
                        email: '',
                        _id: ''
                    });
                })
                .catch(err => console.error(err));
                this.fetchStaffs();
        } else {
            fetch('/api/staff',{
                method: 'POST',
                body: JSON.stringify(this.state),
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data)
                    M.toast({html: 'Miembro del Personal Guardado'});
                    this.setState({ 
                        name: '',
                        id_n: '',
                        age: 0,
                        gender: '',
                        birthdate: '',
                        address: '',
                        user: '',
                        password: '',
                        email: ''
                    });
                })
                .catch(err => console.error(err));
                this.fetchStaffs();
        }
        e.preventDefault();
    }

    componentDidMount() {
        this.fetchStaffs();
    }

    fetchStaffs() {
        fetch('/api/staff')
            .then(res => res.json())
            .then(data => {
                this.setState({staff: data});
                console.log(this.state.staff);
            })
    }

    deleteStaff(id) {
        if (confirm('Seguro de querer eliminarlo?')) {   
            fetch(`api/staff/${id}`, {
                method: 'DELETE',
                headers: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                }
            })
                .then(res => res.json())
                .then(data => {
                    console.log(data);
                    M.toast({html: 'Miembro del Personal Eliminado'});
                    this.fetchStaffs();
                })
        }
    }

    editStaff(id) {
           
        fetch(`api/staff/${id}`)         
            .then(res => res.json())
            .then(data => {
                console.log(data)
                this.setState({
                    name: data.name,
                    id_n: data.id_n,
                    age: data.age,
                    gender: data.gender,
                    birthdate: data.birthdate,
                    address: data.address,
                    user: data.user,
                    password: data.password,
                    email: data.email,
                    _id: data._id
                })
            });
    }
    
    handleChange(e){
        const { name, value } = e.target;
        this.setState({
            [name]: value
        });
    }

    render(){
        return (
            <div>
                { /* Navigation */}
                <nav className="light-blue darken 4">
                    <div className="container">
                        <a className="brand-logo" href="/">Volver al Inicio</a>
                    </div>
                </nav>

                <div className="container">
                    <div className="row">
                        <div className="col s4">
                            <div className="card">
                                <div className="card-content">
                                    <form onSubmit={this.addStaff}>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <input name="name" onChange={this.handleChange}
                                                type="text" value={this.state.name} 
                                                placeholder="Nombre del Miembro"/>
                                            </div>
                                        </div>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="id_n" onChange={this.handleChange} 
                                                placeholder="Cedula" 
                                                className="materialize-textarea" value={this.state.id_n}></textarea>
                                            </div>          
                                        </div>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="age" onChange={this.handleChange} 
                                                placeholder="Edad" 
                                                className="materialize-textarea" value={this.state.age}></textarea>
                                            </div>          
                                        </div>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="gender" onChange={this.handleChange} 
                                                placeholder="Genero" 
                                                className="materialize-textarea" value={this.state.gender}></textarea>
                                            </div>          
                                        </div>
                                        
                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="birthdate" onChange={this.handleChange} 
                                                placeholder="Fecha de Nacimiento del Miembro" 
                                                className="materialize-textarea" value={this.state.birthdate}></textarea>
                                            </div>          
                                        </div>
                                        
                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="address" onChange={this.handleChange} 
                                                placeholder="Direccion" 
                                                className="materialize-textarea" value={this.state.address}></textarea>
                                            </div>          
                                        </div>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="user" onChange={this.handleChange} 
                                                placeholder="Usuario del Miembro del Personal" 
                                                className="materialize-textarea" value={this.state.user}></textarea>
                                            </div>          
                                        </div>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="password" onChange={this.handleChange} 
                                                placeholder="Contraseña del Miembro" 
                                                className="materialize-textarea" value={this.state.password}></textarea>
                                            </div>          
                                        </div>

                                        <div className="row">
                                            <div className="input-field cols12">
                                                <textarea name="email" onChange={this.handleChange} 
                                                placeholder="Correo Electronico" 
                                                className="materialize-textarea" value={this.state.email}></textarea>
                                            </div>          
                                        </div>

                                        <button type="submit" className="btn light-blue darken 4">
                                                Guardar
                                        </button>
                                    </form>
                                </div>
                            </div>
                        
                        </div>
                        <div className="col s14">
                        
                        <table>
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Cedula</th>
                                    <th>Edad</th>
                                    <th>Genero</th>
                                    <th>Fecha de Nacimiento</th>
                                    <th>Direccion</th>
                                    <th>Usuario</th>
                                    <th>Contraseña</th>
                                    <th>Correo</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.staff.map(staff => {
                                        return (
                                            <tr key={staff._id}>
                                                <td>{staff.name}</td>
                                                <td>{staff.id_n}</td>
                                                <td>{staff.age}</td>
                                                <td>{staff.gender}</td>
                                                <td>{staff.birthdate}</td>
                                                <td>{staff.address}</td>
                                                <td>{staff.user}</td>
                                                <td>{staff.password}</td>
                                                <td>{staff.email}</td>
                                                <td>
                                                    <button className="btn light-blue darken 4"
                                                    onClick={() => this.deleteStaff(staff._id)}>
                                                        <i className="material-icons">delete</i>
                                                    </button>
                                                    <button onClick={() => this.editStaff(staff._id)} 
                                                    className="btn light-blue darken 4"
                                                    style={{margin:'4px'}}>
                                                        <i className="material-icons">edit</i>
                                                    </button>
                                                </td>
                                            </tr>
                                        )
                                    })
                                }
                            </tbody>

                        </table>

                        </div>

                    </div>
                </div>
            </div>
        )
    }

}

export default StaffApp;