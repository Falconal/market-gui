const mongoose = require('mongoose');
const { Schema } = mongoose;

const StaffSchema =  new Schema({
    name: { type: String, required: true },
    id_n: { type: String, required: true },
    age: { type: Number, required: true },
    gender: { type: String, required: true },
    birthdate: { type: String, required: true }, 
    address: { type: String, required: true },
    user: { type: String, required:true },
    password: { type: String, required: true},
    email: { type: String, required: true}
});

module.exports = mongoose.model('Staff', StaffSchema);